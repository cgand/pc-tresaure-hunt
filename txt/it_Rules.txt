Add translation for Treasure Hunt rules:


Regole:• Dai una carta indizio al giocatore e nascondi le carte successive nei luoghi indicati dalla carta pecedente, creando una sequenza di indizi fino alla carta del tesoro.• Il giocatore deve trovare il tesoro seguendo gli indizi.Nota: le carte Persona e Topolino indicano qualcuno che ha informazioni su dove si trova la prossima carta indizio, e le fornisce sotto forma di un suggerimento, di un indovinello o di una storia. Con la carta Corri il giocatore viene inseguito e non deve farsi prendere.
